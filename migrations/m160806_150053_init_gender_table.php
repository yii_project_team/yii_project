<?php

use yii\db\Migration;

class m160806_150053_init_gender_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'gender',
			[
				'id' => 'pk',
				'type' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('gender');
    }

}
<?php

use yii\db\Migration;

class m160806_145133_init_services_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'service',
			[
				'id' => 'pk',
				'name' => 'string',
				'description' => 'string',
				'duration' => 'string',
				'price' => 'integer',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('service');
    }

}
<?php

use yii\db\Migration;

class m160806_144935_init_supplier_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'supplier',
			[
				'id' => 'pk',
				'company_name' => 'string',
				'contact_firstname' => 'string',
				'contact_lastname' => 'string',
				'email' => 'string',
				'phone' => 'string',
				'supplier_category' => 'integer',
				'shipping_cost' => 'double',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('supplier');
    }

}
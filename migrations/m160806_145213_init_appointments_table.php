<?php

use yii\db\Migration;

class m160806_145213_init_appointments_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'appointment',
			[
				'id' => 'pk',
				'service' => 'integer',
				'user' => 'integer',
				'status' => 'integer',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('appointment');
    }

}
<?php

use yii\db\Migration;

class m160806_145052_init_product_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'product',
			[
				'id' => 'pk',
				'name' => 'string',
				'description' => 'string',
				'min_order' => 'integer',
				'price' => 'double',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('product');
    }

}
<?php

use yii\db\Migration;

class m160806_144555_init_user_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'user',
			[
				'id' => 'pk',
				'firstname' => 'string',
				'lastname' => 'string',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string',
				'email' => 'string',
				'phone' => 'string',
				'gender' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('user');
    }

}

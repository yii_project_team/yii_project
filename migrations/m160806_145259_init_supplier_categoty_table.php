<?php

use yii\db\Migration;

class m160806_145259_init_supplier_categoty_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'supplier_categoty',
			[
				'id' => 'pk',
				'name' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('supplier_categoty');
    }

}
<?php

use yii\db\Migration;

class m160806_145315_init_status_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'status',
			[
				'id' => 'pk',
				'name' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('status');
    }

}
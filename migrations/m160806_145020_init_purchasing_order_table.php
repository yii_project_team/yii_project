<?php

use yii\db\Migration;

class m160806_145020_init_purchasing_order_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'purchasing_order',
			[
				'id' => 'pk',
				'supply_date' => 'integer',
				'supplier' => 'integer',
				'product' => 'integer',
				'quantity' => 'integer',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer',
			]
		);
    }

    public function down()
    {
		$this->dropTable('purchasing_order');
    }

}
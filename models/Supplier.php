<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $contact_firstname
 * @property string $contact_lastname
 * @property string $email
 * @property string $phone
 * @property integer $supplier_category
 * @property double $shipping_cost
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplier_category', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['shipping_cost'], 'number'],
            [['company_name', 'contact_firstname', 'contact_lastname', 'email', 'phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Company Name',
            'contact_firstname' => 'Contact Firstname',
            'contact_lastname' => 'Contact Lastname',
            'email' => 'Email',
            'phone' => 'Phone',
            'supplier_category' => 'Supplier Category',
            'shipping_cost' => 'Shipping Cost',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}

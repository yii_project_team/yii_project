<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchasing_order".
 *
 * @property integer $id
 * @property integer $supply_date
 * @property integer $supplier
 * @property integer $product
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Purchasing_Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchasing_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supply_date', 'supplier', 'product', 'quantity', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supply_date' => 'Supply Date',
            'supplier' => 'Supplier',
            'product' => 'Product',
            'quantity' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
